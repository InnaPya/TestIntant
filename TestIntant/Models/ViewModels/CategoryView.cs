﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace TestIntant.Models.ViewModels
{
    public class CategoryView
    {
        [Required]
        public String Name { get; set; }

        public IFormFile Image { get; set; }
    }
}
