﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestIntant.Models.ViewModels
{
    public class ProductFilters
    {
        public List<ProductView> Products { get; set; }

        public List<ParameterView> Parameters { get; set; }
    }
}
