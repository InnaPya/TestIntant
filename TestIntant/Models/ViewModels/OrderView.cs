﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TestIntant.Models.ViewModels
{
    public class OrderView
    {
        [Required]
        public String Name { get; set; }

        [Required]
        public String Address { get; set; }

        public List<OrderItemView> Products { get; set; }
    }
}
