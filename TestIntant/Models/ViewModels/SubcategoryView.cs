﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace TestIntant.Models.ViewModels
{
    public class SubcategoryView
    {
        [Required]
        public String Name { get; set; }
        
        public IFormFile Image { get; set; }

        public Guid CategoryId { get; set; }
    }
}
