﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TestIntant.Models.ViewModels
{
    public class ParameterView
    {
        public Guid Id { get; set; }
    
        public String Name { get; set; }

        public Boolean Range { get; set; }

        public String Values { get; set; }

        public SelectList ValueList { get; set; }

        public List<String> ValueEdit { get; set; }

        public String Max { get; set; }

        public String Min { get; set; }
    }
}
