﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace TestIntant.Models.ViewModels
{
    public class ProductView
    {
        [Required]
        public String Name { get; set; }
        
        public IFormFile Image { get; set; }

        public Guid SubcategoryId { get; set; }

        public List<ParameterView> Parameters { get; set; }

        public Boolean InCart { get; set; }

        public String ImagePath { get; set; }

        public Guid Id { get; set; }
    }
}
