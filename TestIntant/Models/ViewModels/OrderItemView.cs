﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestIntant.Models.ViewModels
{
    public class OrderItemView
    {
        public String Product { get; set; }

        public Int32 Quantity { get; set; }
    }
}
