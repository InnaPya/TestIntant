﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestIntant.Models
{
    public class ParameterValue
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid ParameterId { get; set; }

        public Parameter Parameter { get; set; }

        public String Value { get; set; }
    }
}
