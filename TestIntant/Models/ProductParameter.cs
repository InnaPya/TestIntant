﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TestIntant.Models
{
    public class ProductParameter
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        public Guid ProductId { get; set; }

        public Product Product { get; set; }

        [Required]
        public Guid ParameterId { get; set; }

        public Parameter Parameter { get; set; }

        [Required]
        public String Value { get; set; }
    }
}
