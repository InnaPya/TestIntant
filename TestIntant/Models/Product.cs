﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TestIntant.Models
{
    public class Product
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        public String Name { get; set; }

        public String Image { get; set; }

        [Required]
        public Guid SubcategoryId { get; set; }

        public Subcategory Subcategory { get; set; }
    }
}
