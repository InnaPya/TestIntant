﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestIntant.Models
{
    public class Order
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public DateTime Date { get; set; }

        public String Name { get; set; }

        public String Address { get; set; }
    }
}
