﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TestIntant.Models
{
    public class OrderItem
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        public Guid ProductId { get; set; }

        public Product Product { get; set; }

        [Required]
        public Int32 Quantity { get; set; }

        public Guid OrderId { get; set; }

        public Order Order { get; set; }
    }
}
