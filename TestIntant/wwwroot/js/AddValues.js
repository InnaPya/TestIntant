﻿document.addEventListener('DOMContentLoaded', () => {

    window.onload = function () {
        if ($('#Values').is(':checked')) {
            $('#valueButton').css({ 'display': "block" });
        }
    }
    
    $('#Range').click(function () {
        if ($('#Range').is(':checked'))
        {
            $('#valueButton').css({ 'display': "none" });
            $('#createValue').find("input").remove();
        }
    });

    $('#Values').click(function () {
        if ($('#Values').is(':checked'))
        {
            $('#valueButton').css({ 'display': "block" });
        }
    });

    $('#valueButton').click(function () {
        var input = document.createElement('input');
        input.className = "form-control";
        //var remove = document.createElement('span');
        //remove.className = "glyphicon glyphicon-remove";
        //remove.css({ 'visibility': "hidden" });
        $('#createValue').append(input);
        //$('#createValue').append(remove);
    });

    //$('#add').click(function () {
    //    var values = "";
    //    var inputs = $('#createValue').find("input");
    //    inputs.each(function () {
    //        if (this.val() != "") {
    //          values += this.val() + ",";
    //        }
    //    });
    //    $('#hidden').val() = values;
    //});

    document.getElementById("add").addEventListener('click', function() {
        var values = "";
        var inputs = document.getElementById("createValue").getElementsByTagName("input");
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].value != "") {
                values += inputs[i].value +", ";
            }
        }
        if (document.getElementById("valueButton").style.display == "block") {
            document.getElementById("hidden").value = values;
        }
    });

});