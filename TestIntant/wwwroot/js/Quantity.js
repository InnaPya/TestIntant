﻿document.addEventListener('DOMContentLoaded', () => {

    var inc = document.getElementsByClassName("increase");
    for (var i = 0; i < inc.length; i++) {
        inc[i].addEventListener('click', function () {
            var quantity = parseInt(this.nextElementSibling.innerText);
            this.nextElementSibling.innerText = quantity + 1;
            var id = this.parentElement.getElementsByClassName("getId")[0].innerText;
            document.getElementById(id).value = quantity + 1;
        });
    }

    var dec = document.getElementsByClassName("decrease");
    for (var i = 0; i < dec.length; i++) {
        dec[i].addEventListener('click', function () {
            var quantity = parseInt(this.previousElementSibling.innerText);
            if (quantity > 1)
            {
                this.previousElementSibling.innerText = quantity - 1;
                document.getElementById("hidden").value = quantity - 1;
            }
        });
    }
        
});