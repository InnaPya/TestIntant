﻿document.addEventListener('DOMContentLoaded', () => {

    function readURL(e) {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            $(reader).load(function (e) {
                $('#upload-img').attr('src', e.target.result);
            });
            reader.readAsDataURL(this.files[0]);
        }
    }
    $("#upload").change(readURL);

});
