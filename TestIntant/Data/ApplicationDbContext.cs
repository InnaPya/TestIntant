﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TestIntant.Models;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TestIntant.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Subcategory> Subcategories { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Parameter> Parameters { get; set; }

        public DbSet<CartItem> CartItems { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderItem> OrderItems { get; set; }

        public DbSet<ParameterValue> ParameterValues { get; set; }

        public DbSet<ProductParameter> ProductParameters { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Product>().HasOne(x => x.Subcategory).WithMany().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Subcategory>().HasOne(x => x.Category).WithMany().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ProductParameter>().HasOne(x => x.Product).WithMany().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ProductParameter>().HasOne(x => x.Parameter).WithMany().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<CartItem>().HasOne(x => x.Product).WithMany().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<OrderItem>().HasOne(x => x.Product).WithMany().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<OrderItem>().HasOne(x => x.Order).WithMany().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ParameterValue>().HasOne(x => x.Parameter).WithMany().OnDelete(DeleteBehavior.Cascade);
        }
    }
}
