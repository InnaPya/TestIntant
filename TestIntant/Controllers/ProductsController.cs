﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestIntant.Data;
using TestIntant.Models;
using TestIntant.Models.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;

namespace TestIntant.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment hostingEnvironment;

        public ProductsController(ApplicationDbContext context, IHostingEnvironment HostingEnvironment)
        {
            hostingEnvironment = HostingEnvironment;
            _context = context;
        }

        public async Task<IActionResult> Index(Guid subcategoryId, ProductFilters productFilters)
        {
            var applicationDbContext = _context.Products.Where(p => p.SubcategoryId == subcategoryId).Include(p => p.Subcategory);
            ViewBag.SubcategoryId = subcategoryId;

            var subcategory = _context.Subcategories.SingleOrDefault(c => c.Id == subcategoryId);
            ViewBag.Subcategory = subcategory.Name;

            var products = _context.Products.Where(p => p.SubcategoryId == subcategoryId);
            var cartItems = await _context.CartItems.ToListAsync();
            
            if (productFilters.Parameters == null || productFilters.Products == null)
            {
                productFilters.Products = new List<ProductView>();
                foreach (var product in products)
                {
                    ProductView productView = new ProductView();
                    productView.Name = product.Name;
                    productView.SubcategoryId = product.SubcategoryId;
                    productView.ImagePath = product.Image;
                    productView.Id = product.Id;
                    foreach (var item in cartItems)
                    {
                        if (item.ProductId == product.Id)
                        {
                            productView.InCart = true;
                            break;
                        }
                    }
                    if (productView.InCart != true)
                    {
                        productView.InCart = false;
                    }
                    productFilters.Products.Add(productView);
                }                
            }
            else
            {
                for (var i = 0; i < productFilters.Products.Count; i++)
                {
                    foreach (var parameter in productFilters.Parameters)
                    {
                        if (parameter.Range == true && (parameter.Max != null || parameter.Min != null))
                        {
                            var product = productFilters.Products[i];
                            var productParameter = _context.ProductParameters.SingleOrDefault(p => p.ParameterId == parameter.Id && p.ProductId == product.Id);
                            if (productParameter != null)
                            {
                                var value = productParameter.Value;
                                if (parameter.Min != null)
                                {
                                    if (Convert.ToInt32(value) < Convert.ToInt32(parameter.Min))
                                    {
                                        productFilters.Products.Remove(product); i--;
                                        break;
                                    }
                                }
                                if (parameter.Max != null)
                                {
                                    if (Convert.ToInt32(value) > Convert.ToInt32(parameter.Max))
                                    {
                                        productFilters.Products.Remove(product); i--;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                productFilters.Products.Remove(product); i--;
                                break;
                            }
                        }
                        if (parameter.Range == false && parameter.Values != null)
                        {
                            var product = productFilters.Products[i];
                            var productParameter = _context.ProductParameters.SingleOrDefault(p => p.ParameterId == parameter.Id && p.ProductId == product.Id);
                            if (productParameter != null)
                            {
                                var value = productParameter.Value;
                                if (value != parameter.Values)
                                {
                                    productFilters.Products.Remove(product); i--;
                                    break;
                                }
                            }
                            else
                            {
                                productFilters.Products.Remove(product); i--;
                                break;
                            }
                        }
                    }
                }
            }
            productFilters.Parameters = new List<ParameterView>();
            var parameters = _context.Parameters.ToList();
            foreach (var parameter in parameters)
            {
                var values = _context.ParameterValues.Where(v => v.ParameterId == parameter.Id);
                ParameterView parameterView = new ParameterView();
                parameterView.Name = parameter.Name;
                parameterView.Range = parameter.Range;
                parameterView.Id = parameter.Id;
                if (parameter.Range == false)
                {
                    parameterView.ValueList = new SelectList(values, "Value", "Value");
                }
                productFilters.Parameters.Add(parameterView);
            }
            ViewBag.SubcategoryId = subcategory.Id;
            return View(productFilters);
        }
        
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .Include(p => p.Subcategory)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            ProductView productView = new ProductView();
            productView.Parameters = new List<ParameterView>();
            productView.Name = product.Name;
            ViewBag.ImagePath = product.Image;
            productView.SubcategoryId = product.SubcategoryId;
            var productParameters = _context.ProductParameters.Where(p => p.ProductId == id).ToList();
            foreach (var productParameter in productParameters)
            {
                ParameterView parameterView = new ParameterView();
                var parameter = _context.Parameters.SingleOrDefault(p => p.Id == productParameter.ParameterId);
                parameterView.Name = productParameter.Parameter.Name;
                parameterView.Values = productParameter.Value;
                productView.Parameters.Add(parameterView);
            }
            var subcategory = _context.Subcategories.SingleOrDefault(s => s.Id == product.SubcategoryId);
            var category = _context.Categories.SingleOrDefault(c => c.Id == subcategory.CategoryId);
            ViewBag.ProductId = product.Id;
            ViewBag.Subcategory = subcategory.Name;
            ViewBag.Category = category.Name;
            return View(productView);
        }
        
        public IActionResult Create(Guid subcategoryId)
        {
            ProductView model = new ProductView();
            model.Parameters = new List<ParameterView>();
            var parameters = _context.Parameters.ToList();
            foreach (var parameter in parameters)
            {
                var values = _context.ParameterValues.Where(v => v.ParameterId == parameter.Id);
                ParameterView parameterView = new ParameterView();
                parameterView.Id = parameter.Id;
                parameterView.Name = parameter.Name;
                parameterView.Range = parameter.Range;
                if (parameter.Range == false)
                {
                    parameterView.ValueList = new SelectList(values, "Value", "Value");
                }
                model.Parameters.Add(parameterView);
            }
            model.SubcategoryId = subcategoryId;
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductView model)
        {
            if (ModelState.IsValid)
            {
                Product product = new Product();
                product.Name = model.Name;
                product.SubcategoryId = model.SubcategoryId;
                if (model.Image != null)
                {
                    var fileName = Path.GetFileName(Convert.ToString(ContentDispositionHeaderValue.Parse(model.Image.ContentDisposition).FileName).Trim('"'));
                    var fileExt = Path.GetExtension(fileName);
                    var path = Path.Combine(this.hostingEnvironment.WebRootPath, "images", product.Id.ToString("N") + fileExt);
                    product.Image = $"/images/{product.Id:N}{fileExt}";
                    using (var fileStream = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.Read))
                    {
                        await model.Image.CopyToAsync(fileStream);
                    }
                }
                _context.Add(product);
                var parameters = model.Parameters;
                foreach (var parameter in parameters)
                {
                    ProductParameter productParameter = new ProductParameter();
                    productParameter.ParameterId = parameter.Id;
                    productParameter.ProductId = product.Id;
                    productParameter.Value = parameter.Values;
                    _context.ProductParameters.Add(productParameter);
                }
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { subcategoryId = product.SubcategoryId });
            }

            return View(model);
        }
        
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products.SingleOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            ViewBag.Id = id;
            ViewBag.ImagePath = product.Image;
            ProductView model = new ProductView();
            model.Name = product.Name;
            model.SubcategoryId = product.SubcategoryId;
            model.Parameters = new List<ParameterView>();
            var productParameters = _context.ProductParameters.Where(p => p.ProductId == product.Id);
            foreach(var productParameter in productParameters)
            {
                ParameterView parameterView = new ParameterView();
                var parameter = _context.Parameters.SingleOrDefault(p => p.Id == productParameter.ParameterId);
                parameterView.Range = parameter.Range;
                parameterView.Name = parameter.Name;
                parameterView.Id = parameter.Id;
                if (parameterView.Range == false)
                {
                    parameterView.ValueList = new SelectList(_context.ParameterValues.Where(p => p.ParameterId == productParameter.ParameterId), "Value", "Value", productParameter.Value);
                }
                else
                {
                    parameterView.Values = productParameter.Value;
                }
                model.Parameters.Add(parameterView);
            }
            var subcategory = _context.Subcategories.SingleOrDefault(s => s.Id == product.SubcategoryId);
            ViewBag.Subcategories = new SelectList(_context.Subcategories.Where(s => s.CategoryId == subcategory.CategoryId), "Id", "Name", product.SubcategoryId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, ProductView model)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var product = await _context.Products.SingleOrDefaultAsync(m => m.Id == id);
                    product.Name = model.Name;
                    product.SubcategoryId = model.SubcategoryId;
                    if (model.Image != null)
                    {
                        if (product.Image != null) System.IO.File.Delete(Path.Combine(hostingEnvironment.WebRootPath, "images", product.Id.ToString("N") + Path.GetExtension(product.Image)));
                        var fileName = Path.GetFileName(Convert.ToString(ContentDispositionHeaderValue.Parse(model.Image.ContentDisposition).FileName).Trim('"'));
                        var fileExt = Path.GetExtension(fileName);
                        var path = Path.Combine(this.hostingEnvironment.WebRootPath, "images", product.Id.ToString("N") + fileExt);
                        product.Image = $"/subcategory/{product.Id:N}{fileExt}";
                        using (var fileStream = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.Read))
                        {
                            await model.Image.CopyToAsync(fileStream);
                        }
                    }
                    var parameters = model.Parameters;
                    foreach (var parameter in parameters)
                    {
                        var productParameter = _context.ProductParameters.SingleOrDefault(p => p.ParameterId == parameter.Id && p.ProductId == id);
                        productParameter.Value = parameter.Values;
                        _context.Update(productParameter);
                    }
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                    var subcategory = await _context.Subcategories.SingleOrDefaultAsync(s => s.Id == product.SubcategoryId);
                    return RedirectToAction("Index", new { subcategoryId = subcategory.Id });
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            var product1 = await _context.Products.SingleOrDefaultAsync(m => m.Id == id);
            ViewBag.Id = id;
            ViewBag.ImagePath = product1.Image;            
            var subcategory1 = _context.Subcategories.SingleOrDefault(s => s.Id == product1.SubcategoryId);
            ViewBag.Subcategories = new SelectList(_context.Subcategories.Where(s => s.CategoryId == subcategory1.CategoryId), "Id", "Name", product1.SubcategoryId);

            return View(model);
        }
        
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .Include(p => p.Subcategory)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var product = await _context.Products.SingleOrDefaultAsync(m => m.Id == id);
            var subcategory = await _context.Subcategories.SingleOrDefaultAsync(c => c.Id == product.SubcategoryId);
            if (product.Image != null) System.IO.File.Delete(Path.Combine(hostingEnvironment.WebRootPath, "images", product.Id.ToString("N") + Path.GetExtension(product.Image)));
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { subcategoryId = subcategory.Id });
        }

        private bool ProductExists(Guid id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}
