﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestIntant.Data;
using TestIntant.Models;
using TestIntant.Models.ViewModels;

namespace TestIntant.Controllers
{
    public class OrdersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public OrdersController(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public async Task<IActionResult> Index()
        {
            return View(await _context.Orders.ToListAsync());
        }
        
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .SingleOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            OrderView model = new OrderView();
            model.Name = order.Name;
            model.Address = order.Address;
            model.Products = new List<OrderItemView>();
            var items = _context.OrderItems.Where(o => o.OrderId == order.Id);
            foreach (var item in items)
            {
                OrderItemView itemView = new OrderItemView();
                var productName = _context.Products.SingleOrDefault(p => p.Id == item.ProductId).Name;
                itemView.Product = productName;
                itemView.Quantity = item.Quantity;
                model.Products.Add(itemView);
            }
            return View(model);
        }

        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OrderView model)
        {
            if (ModelState.IsValid)
            {
                Order order = new Order();
                order.Name = model.Name;
                order.Address = model.Address;
                order.Date = DateTime.Now;
                var items = _context.CartItems.ToList();
                foreach (var item in items)
                {
                    OrderItem orderItem = new OrderItem();
                    orderItem.OrderId = order.Id;
                    orderItem.ProductId = item.ProductId;
                    orderItem.Quantity = item.Quantity;
                    _context.OrderItems.Add(orderItem);
                    _context.CartItems.Remove(item);
                }
                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Categories");
            }
            return View(model);
        }
        
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .SingleOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var order = await _context.Orders.SingleOrDefaultAsync(m => m.Id == id);
            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(Guid id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }
    }
}
