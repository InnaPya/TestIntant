﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestIntant.Data;
using TestIntant.Models;
using TestIntant.Models.ViewModels;

namespace TestIntant.Controllers
{
    public class CartItemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CartItemsController(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.CartItems.Include(c => c.Product);
            return View(await applicationDbContext.ToListAsync());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(List<CartItem> items)
        {
            var cartItems = await _context.CartItems.ToListAsync();
            foreach(var cartItem in cartItems)
            {
                foreach(var item in items)
                {
                    if (cartItem.ProductId == item.ProductId)
                    {
                        cartItem.Quantity = item.Quantity;
                        _context.CartItems.Update(cartItem);
                        break;
                    }
                }
            }
            await _context.SaveChangesAsync();
            return RedirectToAction("Create", "Orders");
        }

        public async Task<IActionResult> Create(Guid productId)
        {
            if (productId == Guid.Empty || productId == null)
            {
                return NotFound();
            }

            var product = _context.Products.SingleOrDefault(p => p.Id == productId);
            CartItem cartItem = new CartItem();
            cartItem.ProductId = productId;
            cartItem.Quantity = 1;
            _context.CartItems.Add(cartItem);
            await _context.SaveChangesAsync();

            cartItem.ProductId = productId;
            return RedirectToAction("Index", "Products", new { subcategoryId = product.SubcategoryId });
        }
        
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cartItem = await _context.CartItems
                .Include(c => c.Product)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cartItem == null)
            {
                return NotFound();
            }

            _context.CartItems.Remove(cartItem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CartItemExists(Guid id)
        {
            return _context.CartItems.Any(e => e.Id == id);
        }
    }
}
