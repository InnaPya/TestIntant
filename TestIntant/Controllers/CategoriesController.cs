﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestIntant.Data;
using TestIntant.Models;
using TestIntant.Models.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;

namespace TestIntant.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment hostingEnvironment;

        public CategoriesController(ApplicationDbContext context, IHostingEnvironment HostingEnvironment)
        {
            hostingEnvironment = HostingEnvironment;
            _context = context;
        }
        
        public async Task<IActionResult> Index()
        {
            return View(await _context.Categories.ToListAsync());
        }
                
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CategoryView model)
        {
            if (ModelState.IsValid)
            {
                Category category = new Category();
                category.Name = model.Name;
                if (model.Image != null)
                {
                    var fileName = Path.GetFileName(Convert.ToString(ContentDispositionHeaderValue.Parse(model.Image.ContentDisposition).FileName).Trim('"'));
                    var fileExt = Path.GetExtension(fileName);
                    var path = Path.Combine(this.hostingEnvironment.WebRootPath, "images", category.Id.ToString("N") + fileExt);
                    category.Image = $"/images/{category.Id:N}{fileExt}";
                    using (var fileStream = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.Read))
                    {
                        await model.Image.CopyToAsync(fileStream);
                    }
                }
                _context.Add(category);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }
        
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await _context.Categories.SingleOrDefaultAsync(m => m.Id == id);
            if (category == null)
            {
                return NotFound();
            }
            ViewBag.Id = id;
            ViewBag.ImagePath = category.Image;
            CategoryView model = new CategoryView();
            model.Name = category.Name;
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, CategoryView model)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var category = await _context.Categories.SingleOrDefaultAsync(m => m.Id == id);
                    category.Name = model.Name;
                    if (model.Image != null)
                    {
                        if (category.Image != null) System.IO.File.Delete(Path.Combine(hostingEnvironment.WebRootPath, "images", category.Id.ToString("N") + Path.GetExtension(category.Image)));
                        var fileName = Path.GetFileName(Convert.ToString(ContentDispositionHeaderValue.Parse(model.Image.ContentDisposition).FileName).Trim('"'));
                        var fileExt = Path.GetExtension(fileName);
                        var path = Path.Combine(this.hostingEnvironment.WebRootPath, "images", category.Id.ToString("N") + fileExt);
                        category.Image = $"/images/{category.Id:N}{fileExt}";
                        using (var fileStream = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.Read))
                        {
                            await model.Image.CopyToAsync(fileStream);
                        }
                    }
                    _context.Update(category);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoryExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            var category1 = await _context.Categories.SingleOrDefaultAsync(m => m.Id == id);
            ViewBag.Id = id;
            ViewBag.ImagePath = category1.Image;
            return View(model);
        }
        
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await _context.Categories
                .SingleOrDefaultAsync(m => m.Id == id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var category = await _context.Categories.SingleOrDefaultAsync(m => m.Id == id);
            if (category.Image != null) System.IO.File.Delete(Path.Combine(hostingEnvironment.WebRootPath, "images", category.Id.ToString("N") + Path.GetExtension(category.Image)));
            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CategoryExists(Guid id)
        {
            return _context.Categories.Any(e => e.Id == id);
        }
    }
}
