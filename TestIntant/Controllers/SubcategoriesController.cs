﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestIntant.Data;
using TestIntant.Models;
using TestIntant.Models.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;

namespace TestIntant.Controllers
{
    public class SubcategoriesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment hostingEnvironment;

        public SubcategoriesController(ApplicationDbContext context, IHostingEnvironment HostingEnvironment)
        {
            hostingEnvironment = HostingEnvironment;
            _context = context;
        }

        public async Task<IActionResult> Index(Guid categoryId)
        {
            var applicationDbContext = _context.Subcategories.Where(s => s.CategoryId == categoryId).Include(s => s.Category);
            ViewBag.CategoryId = categoryId;
            var category = _context.Categories.SingleOrDefault(c => c.Id == categoryId);
            ViewBag.Category = category.Name;
            return View(await applicationDbContext.ToListAsync());
        }
        
        
        public IActionResult Create(Guid categoryId)
        {
            SubcategoryView model = new SubcategoryView();
            model.CategoryId = categoryId;
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SubcategoryView model)
        {
            if (ModelState.IsValid)
            {
                Subcategory subcategory = new Subcategory();
                subcategory.Name = model.Name;
                subcategory.CategoryId = model.CategoryId;
                if (model.Image != null)
                {
                    var fileName = Path.GetFileName(Convert.ToString(ContentDispositionHeaderValue.Parse(model.Image.ContentDisposition).FileName).Trim('"'));
                    var fileExt = Path.GetExtension(fileName);
                    var path = Path.Combine(this.hostingEnvironment.WebRootPath, "images", subcategory.Id.ToString("N") + fileExt);
                    subcategory.Image = $"/images/{subcategory.Id:N}{fileExt}";
                    using (var fileStream = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.Read))
                    {
                        await model.Image.CopyToAsync(fileStream);
                    }
                }
                _context.Add(subcategory);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { categoryId = subcategory.CategoryId });
            }
            return View(model);
        }
        
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subcategory = await _context.Subcategories.SingleOrDefaultAsync(m => m.Id == id);
            if (subcategory == null)
            {
                return NotFound();
            }
            ViewBag.Id = id;
            ViewBag.ImagePath = subcategory.Image;
            SubcategoryView model = new SubcategoryView();
            model.Name = subcategory.Name;
            model.CategoryId = subcategory.CategoryId;
            ViewBag.Categories = new SelectList(_context.Categories, "Id", "Name", subcategory.CategoryId);
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, SubcategoryView model)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var subcategory = await _context.Subcategories.SingleOrDefaultAsync(m => m.Id == id);
                    subcategory.Name = model.Name;
                    subcategory.CategoryId = model.CategoryId;
                    if (model.Image != null)
                    {
                        if (subcategory.Image != null) System.IO.File.Delete(Path.Combine(hostingEnvironment.WebRootPath, "images", subcategory.Id.ToString("N") + Path.GetExtension(subcategory.Image)));
                        var fileName = Path.GetFileName(Convert.ToString(ContentDispositionHeaderValue.Parse(model.Image.ContentDisposition).FileName).Trim('"'));
                        var fileExt = Path.GetExtension(fileName);
                        var path = Path.Combine(this.hostingEnvironment.WebRootPath, "images", subcategory.Id.ToString("N") + fileExt);
                        subcategory.Image = $"/subcategory/{subcategory.Id:N}{fileExt}";
                        using (var fileStream = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.Read))
                        {
                            await model.Image.CopyToAsync(fileStream);
                        }
                    }
                    _context.Update(subcategory);
                    await _context.SaveChangesAsync();
                    var category = await _context.Categories.SingleOrDefaultAsync(c => c.Id == subcategory.CategoryId);
                    return RedirectToAction("Index", new { categoryId = category.Id });
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubcategoryExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            ViewBag.Id = id;
            var subcategory1 = _context.Subcategories.SingleOrDefault(s => s.Id == id);
            ViewBag.ImagePath = subcategory1.Image;
            ViewBag.Categories = new SelectList(_context.Categories, "Id", "Name", subcategory1.CategoryId);
            return View(model);
        }
        
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subcategory = await _context.Subcategories
                .Include(s => s.Category)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (subcategory == null)
            {
                return NotFound();
            }

            return View(subcategory);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var subcategory = await _context.Subcategories.SingleOrDefaultAsync(m => m.Id == id);
            var category = await _context.Categories.SingleOrDefaultAsync(c => c.Id == subcategory.CategoryId);
            if (subcategory.Image != null) System.IO.File.Delete(Path.Combine(hostingEnvironment.WebRootPath, "images", subcategory.Id.ToString("N") + Path.GetExtension(subcategory.Image)));
            _context.Subcategories.Remove(subcategory);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { categoryId = category.Id });
        }

        private bool SubcategoryExists(Guid id)
        {
            return _context.Subcategories.Any(e => e.Id == id);
        }
    }
}
