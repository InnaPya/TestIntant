﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestIntant.Data;
using TestIntant.Models;
using TestIntant.Models.ViewModels;

namespace TestIntant.Controllers
{
    public class ParametersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ParametersController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Parameters.ToListAsync());
        }
                
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ParameterView model)
        {
            if (ModelState.IsValid)
            {
                Parameter parameter = new Parameter();
                parameter.Name = model.Name;
                parameter.Range = model.Range;
                if (parameter.Range == false && model.Values != null)
                {
                    var values = model.Values.Split(",");
                    foreach (var value in values)
                    {
                        if (value != "" && value != " ")
                        {
                            ParameterValue parameterValue = new ParameterValue();
                            parameterValue.ParameterId = parameter.Id;
                            value.Trim();
                            parameterValue.Value = value;
                            _context.ParameterValues.Add(parameterValue);
                        }
                    }
                }
                _context.Add(parameter);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }
        
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parameter = await _context.Parameters.SingleOrDefaultAsync(m => m.Id == id);
            if (parameter == null)
            {
                return NotFound();
            }

            ParameterView parameterView = new ParameterView();
            parameterView.Name = parameter.Name;
            parameterView.Range = parameter.Range;
            parameterView.ValueEdit = new List<String>();
            parameterView.Id = parameter.Id;
            var parameterValues = _context.ParameterValues.Where(p => p.ParameterId == id);
            foreach (var parameterValue in parameterValues)
            {
                parameterView.ValueEdit.Add(parameterValue.Value);
            }
            return View(parameterView);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ParameterView model)
        {
            if (model.Id == Guid.Empty)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var parameter = _context.Parameters.SingleOrDefault(p => p.Id == model.Id);
                    parameter.Name = model.Name;
                    var parameterValues = _context.ParameterValues.Where(p => p.ParameterId == parameter.Id);
                    foreach (var parameterValue in parameterValues)
                    {
                        _context.ParameterValues.Remove(parameterValue);
                    }
                    if (model.Range == false && model.Values != null)
                    {
                        var values = model.Values.Split(",");
                        foreach (var value in values)
                        {
                            if (value != "" && value != " ")
                            {
                                ParameterValue parameterValue = new ParameterValue();
                                parameterValue.ParameterId = parameter.Id;
                                value.Trim();
                                parameterValue.Value = value;
                                _context.ParameterValues.Add(parameterValue);
                            }
                        }
                    }
                    parameter.Range = model.Range;
                    _context.Update(parameter);
                    
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ParameterExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }
        
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parameter = await _context.Parameters
                .SingleOrDefaultAsync(m => m.Id == id);
            if (parameter == null)
            {
                return NotFound();
            }

            return View(parameter);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var parameter = await _context.Parameters.SingleOrDefaultAsync(m => m.Id == id);
            _context.Parameters.Remove(parameter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ParameterExists(Guid id)
        {
            return _context.Parameters.Any(e => e.Id == id);
        }
    }
}
